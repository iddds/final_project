class RingTopology(object):

    __instance = None
    nodes = None
    leader = None

    def __str__(self):
        return "Ring Topology with: {}".format(self.nodes.print_list())

    def __new__(cls):
        if RingTopology.__instance is None:
            RingTopology.__instance = object.__new__(cls)
        return RingTopology.__instance

    def register_client(self, pid):
        if self.nodes is None:
            self.nodes = RingNode(pid)
            self.nodes.append_node(self.nodes)  # Making list circular
            self.leader = pid
        else:
            self.nodes.append_node(RingNode(pid))
        return self.leader


class RingNode:

    def __init__(self, pid):
        self.pid = str(pid)
        self.next = None
        self.msg_queue = []

    def __repr__(self):
        return "Server {}".format(self.pid)

    # def __gt__(self, other):
    #     return self.pid > other.pid
    #
    # def __lt__(self, other):
    #     return self.pid < other.pid

    def __eq__(self, other):
        return self.pid == other.pid

    def append_node(self, node):
        tail = self
        while tail.next is not None and tail.next.pid != self.pid:
            tail = tail.next
        tail.next = node
        node.next = self

    def print_list(self):
        ids = [str(self)]
        tail = self.next
        while tail.pid != self.pid:
            ids.append(str(tail))
            # print("Server {} : next {}".format(tail.pid, tail.next))
            tail = tail.next
        return ids

    def find_node(self, pid):
        pid = str(pid)
        tail = self
        while tail.next.pid != self.pid and tail.pid != pid:
            print("Server {} : next {}".format(tail.pid, tail.next))
            tail = tail.next
        return tail if tail.pid == pid else None

    def pull_message(self):
        message = ""
        if len(self.msg_queue) > 0:
            message = self.msg_queue.pop(0)
            print("{} ::: pull message :: {}".format(self, message))
        return message

    def send_request_message(self, message):
        print("Sending message to  ::: {} :: {}".format(self, message))
        self.msg_queue.append(message)

