from semaphore.topology import RingTopology


def test_registration():
    topology = RingTopology()
    topology.register_client(6)
    topology.register_client(10)
    topology.register_client(13)
    print(topology)


test_registration()
