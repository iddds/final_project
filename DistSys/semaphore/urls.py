from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('ajax/register-client', views.register_client, name='register_client'),
    path('ajax/access-resource', views.get_resource, name='access_resource'),
    path('ajax/pull-messages', views.pull_messages, name='pull_messages'),
    path('ajax/request-resource', views.request_resource, name='request_resource'),
    path('ajax/response-request', views.response_request, name='response_request'),
    path('ajax/find-leader', views.find_leader, name='find_leader'),
    path('ajax/advertise-leader', views.find_leader, name='advertise_leader'),
    path('ajax/restart-topology', views.restart_topology, name='restart_topology'),
]
