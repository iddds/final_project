from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from semaphore import rpi_methods as rpi
from semaphore.topology import RingTopology
from django.views.decorators.csrf import csrf_exempt
import json


def index(request):
    pid = request.GET.get("pid", "")
    # lamp1 = request.GET.get("lamp1", None)
    # lamp2 = request.GET.get("lamp2", None)
    # lamp3 = request.GET.get("lamp3", None)
    #
    # if lamp1:
    #     rpi.toggle_led(1)
    # elif lamp2:
    #     rpi.toggle_led(2)
    # elif lamp3:
    #     rpi.toggle_led(3)

    context = {
        "lamp1": rpi.get_lamp_status(1),
        "lamp2": rpi.get_lamp_status(2),
        "lamp3": rpi.get_lamp_status(3),
        "pid": pid
    }

    return render(request, 'semaphore/index.html', context)


def get_resource(request):
    data = {"status": "success", "details": "Resource accessed"}
    try:
        rpi.toggle_led(1)
    except Exception as e:
        data["status"] = "failed"
        data["details"] = e
    return JsonResponse(data)


@csrf_exempt
def register_client(request):
    data = {"status": "", "currentLeader": "Could not be retrieved."}
    print(request.GET)
    pid = request.GET.get("pid", None)
    if pid:
        topology = RingTopology()
        current_leader = topology.register_client(pid)
        data["currentLeader"] = current_leader
        data["status"] = "success"
        data.update({"pid": pid})
        data.update({"topology": str(topology)})
    else:
        data["status"] = "failed. Send Process ID"
    print(data)
    return JsonResponse(data)


def pull_messages(request):
    data = {"status": ""}
    pid = request.GET.get("pid", None)
    if pid:
        topology = RingTopology()
        node = None
        if topology.nodes is not None:
            node = topology.nodes.find_node(pid)
        if node is None:
            return JsonResponse(data)
        data.update({"message": node.pull_message()})
        data["status"] = "success"
        # data.update({"topology": str(topology)})
    else:
        data["status"] = "failed. Send Process ID"
    return JsonResponse(data)


def request_resource(request):
    data = {"status": "Error", "response": "Message Not Sent"}
    message = request.GET.get("message", None)
    if message:
        topology = RingTopology()
        message = json.loads(message)
        leader = topology.nodes.find_node(message["leader"])
        leader.send_request_message(message)
        data["status"] = "success"
        data.update({"response": "Message sent."})
    return JsonResponse(data)


def response_request(request):
    data = {"status": "Error", "response": "Message Not Sent", "lamp": rpi.get_lamp_status(1)}
    message = request.GET.get("message", None)
    if message:
        topology = RingTopology()
        message = json.loads(message)
        client = topology.nodes.find_node(message["destination"])
        client.send_request_message(message)
        data["status"] = "success"
        data.update({"response": "Message sent."})
    return JsonResponse(data)


def find_leader(request):
    data = {"status": "Error", "response": "Message Not Sent"}
    message = request.GET.get("message", None)
    if message:
        topology = RingTopology()
        message = json.loads(message)
        client = topology.nodes.find_node(message["source"])
        client.next.send_request_message(message)
        data["status"] = "success"
        data.update({"response": "Message sent."})
    return JsonResponse(data)


def restart_topology(request):
    data = {"status": "Error", "response": "Topology Not Started"}
    topology = RingTopology()
    topology.nodes = None
    return JsonResponse(data)
